﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using twitch.api.Adapters;

namespace menu.api
{
    public class Menu
    {
        #region Fields
        private string title;
        private IDisplayer displayer;
        private List<Item> items;
        #endregion

        #region Constructors
        public Menu(IDisplayer displayer, string title)
        {
            this.Title = title;
            this.Displayer = displayer;
            this.Items = new();
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Methode d'ajout d'un item dans le menu
        /// </summary>
        /// <param name="item"></param>
        public void AddItem(Item item)
        {
            Items.Add(item);
        }

        /// <summary>
        /// Methode qui permet l'affichage du menu
        /// Tient compte de l'ordre definit pour les item
        /// </summary>
        public void Display()
        {
            displayer.Display(this.Title);
            items.OrderBy(item => item.Order).ToList().ForEach(item => displayer.Display($"{item.Id}. {item.Name}"));
        }
        #endregion

        #region Properties
        public IDisplayer Displayer { get => displayer; set => displayer = value; }
        public List<Item> Items { get => items; set => items = value; }
        public string Title { get => title; set => title = value; }
        #endregion
    }
}
