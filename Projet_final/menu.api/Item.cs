﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace menu.api
{
    public class Item
    {
        #region Fields
        private int id;
        private string name;
        private int order;
        #endregion

        #region Constructors
        public Item(int id, string name, int order)
        {
            this.Id = id;
            this.Name = name;
            this.Order = order;
        }
        #endregion

        #region Public methods
        #endregion

        #region Properties
        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public int Order { get => order; set => order = value; }
        #endregion
    }
}
