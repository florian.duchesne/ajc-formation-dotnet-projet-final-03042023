﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using twitch.api.Adapters;
using twitch.api;
using projet.infrastructure.Console;

namespace menu.api
{
    public class MenuTwitch
    {
        private Dictionary<int, Action> dictActionMenu;
        private Menu menu;

        private IDisplayer displayer;
        private IReader reader;

        private ILoader<Gift, GiftList> giftLoader;
        private ISave<Gift> giftSaver;
        private ILoader<Subscriber, SubscriberList> subLoader;
        private ISave<Subscriber> subSaver;
        private ILoader<Twitcher, TwitcherList> twitcherLoader;
        private ISave<Twitcher> twitcherSaver;

        private JsonFollowersSaver jsonFollowersSaver;
        private JsonStreamerSaver jsonStreamersSaver;
        private JsonGiftsSaver jsonGiftsSaver;

        private bool stop;

        public Dictionary<int, Action> DictActionMenu { get => dictActionMenu; set => dictActionMenu = value; }
        public Menu Menu { get => menu; set => menu = value; }
        public IDisplayer Displayer { get => displayer; set => displayer = value; }
        public IReader Reader { get => reader; set => reader = value; }
        public ILoader<Gift, GiftList> GiftLoader { get => giftLoader; set => giftLoader = value; }
        public ISave<Gift> GiftSaver { get => giftSaver; set => giftSaver = value; }
        public ILoader<Subscriber, SubscriberList> SubLoader { get => subLoader; set => subLoader = value; }
        public ISave<Subscriber> SubSaver { get => subSaver; set => subSaver = value; }
        public ILoader<Twitcher, TwitcherList> TwitcherLoader { get => twitcherLoader; set => twitcherLoader = value; }
        public ISave<Twitcher> TwitcherSaver { get => twitcherSaver; set => twitcherSaver = value; }
        public bool Stop { get => stop; set => stop = value; }

        public MenuTwitch
            (
                IDisplayer displayer,
                IReader reader,
                ILoader<Gift, GiftList> giftLoader,
                ISave<Gift> giftSaver,
                ILoader<Subscriber, SubscriberList> subLoader,
                ISave<Subscriber> subSaver,
                ILoader<Twitcher, TwitcherList> twitcherLoader,
                ISave<Twitcher> twitcherSaver
            )
        {
            this.Displayer = displayer;
            this.Reader = reader;
            this.GiftLoader = giftLoader;
            this.GiftSaver = giftSaver;
            this.SubLoader = subLoader;
            this.SubSaver = subSaver;
            this.TwitcherLoader = twitcherLoader;
            this.TwitcherSaver = twitcherSaver;

            jsonFollowersSaver = new(Path.Combine(Environment.CurrentDirectory, "followers.txt"));
            jsonGiftsSaver = new(Path.Combine(Environment.CurrentDirectory, "gifts.txt"));
            jsonStreamersSaver = new(Path.Combine(Environment.CurrentDirectory, "streamers.txt"));

            Stop = false;

            this.Menu = new(displayer, "==== Twitch ====");
            Item afficherStreamers = new(1, "Afficher streamers", 1);
            Item afficherSubs = new(2, "Afficher subs", 2);
            Item afficherCadeaux = new(3, "Afficher cadeaux", 3);
            Item ajouterSub = new(4, "Ajouter sub", 4);
            Item creerCadeau = new(5, "Creer cadeau", 5);
            Item distribuer = new(6, "Distribuer aleatoirement", 6);
            Item sauvegarder = new(7, "Sauvegarder", 7);
            Item quitter = new(8, "Quitter", 8);


            Menu.AddItem(afficherStreamers);
            Menu.AddItem(afficherSubs);
            Menu.AddItem(afficherCadeaux);
            Menu.AddItem(ajouterSub);
            Menu.AddItem(creerCadeau);
            Menu.AddItem(distribuer);
            Menu.AddItem(sauvegarder);
            Menu.AddItem(quitter);



            DictActionMenu = new()
            {
                {1, AfficherTwitchers },
                {2, AfficherSubs },
                {3, AfficherCadeaux },
                {4, AjouterSub },
                {5, CreerCadeau },
                {6, DistribuerAleatoirement },
                {7, Sauvegarder },
                {8, () => {Stop = true; } }
            };
        }

        public void Run()
        {
            do
            {
                Menu.Display();
                if (int.TryParse(reader.Read(), out int choice))
                {
                    if (dictActionMenu.TryGetValue(choice, out Action actionMenu))
                    {
                        actionMenu();
                    }
                    else
                    {
                        Displayer.Display("Entree invalide.");
                    }
                }
                else
                {
                    Displayer.Display("Entree invalide.");
                }
            } while (!stop);
        }

        public void AfficherTwitchers()
        {
            TwitcherLoader.GetAll().ForEach(t =>
            {
                Displayer.Display(t.ToString());
            });
        }

        public void AfficherSubs()
        {
            TwitcherLoader.GetAll().ForEach(t =>
            {
                Displayer.Display(t.ToString());
                t.Subscribers.ForEach(s =>
                {
                    Displayer.Display(s.ToString());
                });
            });
        }

        public void AfficherCadeaux()
        {
            GiftLoader.GetAll().ForEach(g =>
            {
                Displayer.Display(g.ToString());
            });
        }

        public Twitcher SelectTwitcher()
        {
            var twitchers = TwitcherLoader.GetAll();
            bool validInput;
            int id;
            do
            {
                AfficherTwitchers();
                displayer.Display("Choix du streamer ?");
                if (int.TryParse(reader.Read(), out id))
                {
                    if (twitchers.Any(t => t.Id == id))
                    {
                        validInput = true;
                    }
                    else
                    {
                        Displayer.Display("Entree invalide");
                        validInput = false;
                    }
                }
                else
                {
                    Displayer.Display("Entree invalide");
                    validInput = false;
                }
            } while (!validInput);
            return TwitcherLoader.Get(id);
        }

        public void AjouterSub()
        {
            var twitchers = TwitcherLoader.GetAll();
            if (twitchers.Count > 0)
            {
                var twitcher = SelectTwitcher();

                Displayer.Display("name ?");
                string pseudo = reader.Read();

                Displayer.Display("Email ?");
                string email = reader.Read();

                Subscriber subscriber = new(pseudo, email);

                subscriber.Twitchers.Add(twitcher);
                twitcher.Subscribers.Add(subscriber);

                SubSaver.Add(subscriber);
            }
            else
            {
                Displayer.Display("Aucun subscriber enregistré.");
            }
        }

        public void CreerCadeau()
        {
            var twitchers = TwitcherLoader.GetAll();
            bool validInput;
            if (twitchers.Count > 0)
            {
                var twitcher = SelectTwitcher();
                do
                {
                    displayer.Display("Type de cadeau ?");
                    displayer.Display("1. Réduction (promo sur un site de gaming)");
                    displayer.Display("2. Apprendre à bien jouer sur un jeu");
                    displayer.Display("3. Réduc de 100% sur un jeu vidéo");
                    displayer.Display("4. Passer du temps avec le Streamer");
                    Dictionary<int, Func<Gift>> getNewGift = new()
                    {
                        {1, () => { return new Gift(GiftType.Discount); } },
                        {2, () => { return new Gift(GiftType.GameLesson); } },
                        {3, () => { return new Gift(GiftType.Offer); } },
                        {4, () => { return new Gift(GiftType.StreamerMeeting); } }
                    };
                    if (int.TryParse(Reader.Read(), out int giftCategory))
                    {
                        if (getNewGift.TryGetValue(giftCategory, out Func<Gift> createGift))
                        {
                            Gift newGift = createGift();
                            twitcher.Gifts.Add(newGift);
                            Displayer.Display("Cadeau ajouté !");
                            validInput = true;
                        }
                        else
                        {
                            Displayer.Display("Erreur saisie");
                            validInput = false;
                        }
                    }
                    else
                    {
                        validInput = false;
                    }
                } while (!validInput);
            }
            else
            {
                displayer.Display("Aucun subscriber enregistré.");
            }
        }

        public void DistribuerAleatoirement()
        {
            var gifts = GiftLoader.GetAll();
            var subscribers = SubLoader.GetAll();
            new RandomSelector().dispatch(gifts, subscribers);
        }

            //        this.Displayer = displayer;
            //this.Reader = reader;
            //this.GiftLoader = giftLoader;
            //this.GiftSaver = giftSaver;
            //this.SubLoader = subLoader;
            //this.SubSaver = subSaver;
            //this.TwitcherLoader = twitcherLoader;
            //this.TwitcherSaver = twitcherSaverSubLoader

        public void Sauvegarder()
        {
            var gifts = GiftLoader.GetAll();
            var followers = SubLoader.GetAll();
            var streamer = TwitcherLoader.GetAll();

            jsonGiftsSaver.SaveAll(gifts);
            jsonStreamersSaver.SaveAll(streamer);
            jsonFollowersSaver.SaveAll(followers);

            Displayer.Display("Enregistrement terminé!");
            Displayer.Display($"Streamer: {jsonStreamersSaver.FilePath}");
            Displayer.Display($"Gifts: {jsonGiftsSaver.FilePath}");
            Displayer.Display($"Followers: {jsonFollowersSaver.FilePath}");
        }
    }

}
