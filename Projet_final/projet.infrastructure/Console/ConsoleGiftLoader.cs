﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using twitch.api.Adapters;
using twitch.api;
using projet.infrastructure.Exceptions;

namespace projet.infrastructure.Console
{
    public class ConsoleGiftLoader : ILoader<Gift, GiftList>
    {
        private GiftList gifts;

        public ConsoleGiftLoader(GiftList gifts)
        {
            this.Gifts = gifts;
        }

        public GiftList Gifts { get => gifts; set => gifts = value; }


        public Gift Get(int id)
        {
            var gift = Gifts.FirstOrDefault(gift => gift.Id == id);
            if (gift == null)
            {
                throw new UnknownGiftIndexException();
            }
            return gift;
        }

        public GiftList GetAll()
        {
            return Gifts;
        }
    }
}
