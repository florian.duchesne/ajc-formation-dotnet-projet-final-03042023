﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using twitch.api;
using twitch.api.Adapters;

namespace projet.infrastructure.Console
{
    public class JsonFollowersSaver : ISave<Subscriber>
    {
        private readonly string filePath;

        public JsonFollowersSaver(string filePath)
        {
            this.filePath = filePath;
        }

        public string FilePath => filePath;

        public void Add(Subscriber subscriber)
        {
            string json = JsonConvert.SerializeObject(subscriber, Formatting.Indented);
            File.AppendAllText(FilePath, json);
        }

        public void SaveAll(List<Subscriber> subscribers)
        {
            string json = JsonConvert.SerializeObject(subscribers, Formatting.Indented);
            File.WriteAllText(FilePath, json); ;
        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }
    }
}
