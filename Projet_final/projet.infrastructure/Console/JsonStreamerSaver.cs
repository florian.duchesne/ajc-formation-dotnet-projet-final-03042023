﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using twitch.api;
using twitch.api.Adapters;

namespace projet.infrastructure.Console
{
    public class JsonStreamerSaver : ISave<Twitcher>
    {
        private readonly string filePath;

        public JsonStreamerSaver(string filePath)
        {
            this.filePath = filePath;
        }

        public string FilePath => filePath;

        public void Add(Twitcher twitcher)
        {
            string json = JsonConvert.SerializeObject(twitcher, Formatting.Indented);
            File.AppendAllText(FilePath, json);
        }

        public void SaveAll(List<Twitcher> twitchers)
        {
            string json = JsonConvert.SerializeObject(twitchers, Formatting.Indented);
            File.WriteAllText(FilePath, json);
        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }
    }
}
