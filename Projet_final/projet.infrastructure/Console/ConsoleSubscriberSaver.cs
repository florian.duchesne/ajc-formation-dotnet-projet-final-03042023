﻿using projet.infrastructure.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using twitch.api.Adapters;
using twitch.api;

namespace projet.infrastructure.Console
{
    public class ConsoleSubscriberSaver : ISave<Subscriber>
    {
        private List<Subscriber> subscribers;

        public ConsoleSubscriberSaver(List<Subscriber> subscribers)
        {
            Subscribers = subscribers;
        }

        public List<Subscriber> Subscribers { get => subscribers; set => subscribers = value; }

        public void Add(Subscriber subscriber)
        {
            if (Subscribers.Any(s => s.Id == subscriber.Id))
            {
                throw new DuplicateSubscriberIndexException();
            }
            Subscribers.Add(subscriber);
        }

        public void Remove(int id)
        {
            if (!Subscribers.Any(s => s.Id == id))
            {
                throw new UnknownSubscriberIndexException();
            }
            Subscribers.Remove(Subscribers.FirstOrDefault(gift => gift.Id == id));
        }
    }
}
