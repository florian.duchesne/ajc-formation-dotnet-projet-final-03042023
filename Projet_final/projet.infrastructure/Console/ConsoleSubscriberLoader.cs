﻿using projet.infrastructure.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using twitch.api.Adapters;
using twitch.api;

namespace projet.infrastructure.Console
{
    public class ConsoleSubscriberLoader : ILoader<Subscriber, SubscriberList>
    {
        private SubscriberList subscribers;

        public ConsoleSubscriberLoader(SubscriberList subscribers)
        {
            this.Subscribers = subscribers;
        }
        public SubscriberList Subscribers { get => subscribers; set => subscribers = value; }

        public Subscriber Get(int id)
        {
            var subscriber = Subscribers.FirstOrDefault(subscriber => subscriber.Id == id);
            if (subscriber == null)
            {
                throw new UnknownSubscriberIndexException();
            }
            return subscriber;

        }

        public SubscriberList GetAll()
        {
            return Subscribers;
        }
    }
}
