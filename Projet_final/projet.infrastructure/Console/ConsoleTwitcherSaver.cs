﻿using projet.infrastructure.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using twitch.api.Adapters;
using twitch.api;

namespace projet.infrastructure.Console
{
    public class ConsoleTwitcherSaver : ISave<Twitcher>
    {
        private List<Twitcher> twitchers;

        public ConsoleTwitcherSaver(List<Twitcher> twitchers)
        {
            Twitchers = twitchers;
        }

        public List<Twitcher> Twitchers { get => twitchers; set => twitchers = value; }

        public void Add(Twitcher twitcher)
        {
            if (Twitchers.Any(t => t.Id == twitcher.Id))
            {
                throw new DuplicateTwitcherIndexException();
            }
            Twitchers.Add(twitcher);
        }

        public void Remove(int id)
        {
            if (!Twitchers.Any(s => s.Id == id))
            {
                throw new UnknownSubscriberIndexException();
            }
            Twitchers.Remove(Twitchers.FirstOrDefault(gift => gift.Id == id));
        }
    }
}
