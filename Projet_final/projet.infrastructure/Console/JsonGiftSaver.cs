﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using twitch.api;
using twitch.api.Adapters;

namespace projet.infrastructure.Console
{
    public class JsonGiftsSaver : ISave<Gift>
    {
        private readonly string filePath;

        public string FilePath => filePath;

        public JsonGiftsSaver(string filePath)
        {
            this.filePath = filePath;
        }

        public void Add(Gift gift)
        {
            string json = JsonConvert.SerializeObject(gift, Formatting.Indented);
            File.AppendAllText(FilePath, json);
        }

        public void SaveAll(List<Gift> gifts)
        {
            string json = JsonConvert.SerializeObject(gifts, Formatting.Indented);
            File.WriteAllText(FilePath, json);
        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }
    }
}
