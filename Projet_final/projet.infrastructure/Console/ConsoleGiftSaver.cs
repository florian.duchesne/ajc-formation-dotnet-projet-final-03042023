﻿using projet.infrastructure.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using twitch.api.Adapters;
using twitch.api;

namespace projet.infrastructure.Console
{
    public class ConsoleGiftSaver : ISave<Gift>
    {
        private List<Gift> gifts;

        public ConsoleGiftSaver(List<Gift> gifts)
        {
            this.Gifts = gifts;
        }

        public List<Gift> Gifts { get => gifts; set => gifts = value; }

        public void Add(Gift gift)
        {
            if (Gifts.Any(g => g.Id == gift.Id))
            {
                throw new DuplicateGiftIndexException();
            }
            Gifts.Add(gift);
        }

        public void Remove(int id)
        {
            if (!Gifts.Any(g => g.Id == id))
            {
                throw new UnknownGiftIndexException();
            }
            Gifts.Remove(Gifts.FirstOrDefault(gift => gift.Id == id));
        }
    }
}
