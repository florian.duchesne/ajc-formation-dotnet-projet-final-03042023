﻿using projet.infrastructure.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using twitch.api.Adapters;
using twitch.api;

namespace projet.infrastructure.Console
{
    public class ConsoleTwitcherLoader : ILoader<Twitcher, TwitcherList>
    {
        private TwitcherList twitchers;

        public ConsoleTwitcherLoader(TwitcherList twitchers)
        {
            this.Twitchers = twitchers;
        }

        public TwitcherList Twitchers { get => twitchers; set => twitchers = value; }

        public Twitcher Get(int id)
        {
            var twitcher = Twitchers.FirstOrDefault(twitcher => twitcher.Id == id);
            if (twitcher == null)
            {
                throw new UnknownTwitcherIndexException();
            }
            return twitcher;
        }

        public TwitcherList GetAll()
        {
            return Twitchers;
        }
    }
}
