﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using twitch.api;
using twitch.api.Adapters;
using Newtonsoft.Json;

namespace projet.infrastructure
{
	public class JsonSaver : ISave<Object>
    {
		private readonly string chemin;

		public JsonSaver(string chemin)
		{
			this.chemin = chemin;
		}

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }

        public void Add(object objet)
		{
			string json = JsonConvert.SerializeObject(objet,
			new JsonSerializerSettings
			{
				ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
			});
			File.WriteAllText(this.chemin, json);
		}

		//public void SaveFollower(Subscriber subscriber)
		//{
		//	this.Add(subscriber);
		//}

		//public void SaveFollowers(List<Subscriber> subscribers)
		//{
		//	this.Add(subscribers);
		//}

		//public void SaveGift(Gift gift)
		//{
		//	this.Add(gift);
		//}

		//public void SaveGifts(List<Gift> gifts)
		//{
		//	this.Add(gifts);
		//}
	}
}
