﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api
{
	public class Gift
	{
        private static int count = 0;
        private int id;
        private GiftType category;
        private string description;
		private DateTime creationDate;
		private Subscriber? subscriber;
		private Twitcher twitcher;

        public Gift(GiftType category)
        {
            this.Id = count++;
            this.Category = category;
            this.creationDate = DateTime.Now;
        }

        public Gift(int id, GiftType category)
        {
            this.Id = id;
            this.Category = category;
            this.creationDate = DateTime.Now;
            this.Twitcher = twitcher;
        }

        public Gift(int id, Twitcher twitcher)
		{
			this.Id = id;
			this.creationDate = DateTime.Now;
			this.Twitcher = twitcher;	
		}

        public int Id { get => id; set => id = value; }
        public GiftType Category { get => category; set => category = value; }
        public DateTime CreationDate { get => creationDate; set => creationDate = value; }
        public Subscriber? Subscriber { get => subscriber; set => subscriber = value; }
		public Twitcher Twitcher { get => twitcher; set => twitcher = value; }

		public override string ToString()
		{
            return $"[Gift] {this.Id}. {this.Category}";
        }
    }
}
