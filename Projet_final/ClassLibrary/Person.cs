﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api
{
    public abstract class Person
    {
        private static int count = 1;
        private int id;
        private string name;
        private string email;

        public Person()
        {
            this.Id = count;
            count++;
        }

        public Person(string name, string email)
        {
            this.Id = count;
            this.name = name;
            this.email = email;
            count++;

        }

        public Person(int id, string pseudo, string email)
        {
            this.Id = id;
            this.name = pseudo;
            this.Email = email;

        }

        public override string ToString()
        {
            return $"{this.Id}. {this.name} ";
        }

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Email { get => email; set => email = value; }
    }

}
