﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api
{

    public class Streaming
    {
        private int id;
        private Twitcher streamer;
        private List<Subscriber> subscribersConnected;
        private string title;
        private DateTime? startTime;
        private DateTime? endTime;
        [NotMapped]
        private StatutStream status;
        [NotMapped]
        private Boolean? changeStatus;
        /// <summary>
        /// constructeur toléré par dotnet ef
        /// </summary>
        public Streaming()
        {
            this.subscribersConnected = new List<Subscriber>();
        }

        public Streaming(Twitcher streamer) :this()
        {
            this.streamer = streamer;
        }
        
        public Streaming(Twitcher streamer, string title) : this(streamer)
        {
            this.title = title; 
        }

        public Streaming(Twitcher streamer, DateTime datetime, string title) : this(streamer, title)
        {
            this.startTime = datetime;
        }

        public StatutStream getStatus()
        {

            if (DateTime.Now < this.startTime)
            {
                this.Status = StatutStream.AVenir;
            }
            if (DateTime.Now > this.endTime)
            {
                this.Status = StatutStream.Fini;
            }
            if (DateTime.Now > this.startTime && this.endTime == null)
            {
                this.Status = StatutStream.EnCours;
            }
            return Status;
        }

        public Twitcher Streamer { get => streamer; set => streamer = value; }
        public List<Subscriber> SubscribersConnected { get => subscribersConnected; set => subscribersConnected = value; }
        public DateTime? StartTime { get => startTime; set => startTime = value; }
        public DateTime? EndTime { get => endTime; set => endTime = value; }
        public int Id { get => id; set => id = value; }
        public string Title { get => title; set => title = value; }
        [NotMapped]
        public StatutStream Status { get => status; set => status = value; }
        [NotMapped]
        public bool? ChangeStatus { get => changeStatus; set => changeStatus = value; }
    }

    public enum StatutStream
    {
        EnCours = 1,
        AVenir = 2,
        Fini = 0
    }
}
