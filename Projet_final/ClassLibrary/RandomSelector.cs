﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api
{
	public class RandomSelector : IGiftDispatcher
	{
        private static readonly Random random = new Random();
        public void dispatch(GiftList gifts, SubscriberList subscribers)
        {
            var shuffledFollowers = subscribers.OrderBy(follower => random.Next()).Take(gifts.Count).ToList();
            var shuffledGifts = gifts.OrderBy(gift => random.Next()).Take(subscribers.Count).ToList();
            int index = 0;
            shuffledGifts.ForEach(gift =>
            {
                shuffledFollowers[index].Gifts.Add(gift);
                index++;
            });
            gifts.RemoveAll(gift => shuffledGifts.Contains(gift));
        }
    }

}
