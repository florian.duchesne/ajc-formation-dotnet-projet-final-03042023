﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using twitch.api.Adapters;

namespace twitch.api
{
	public class Twitcher : Person
	{
		List<Subscriber> subscribers = new List<Subscriber>();
		List<Gift> gifts = new List<Gift>();	
		List<Streaming> streams = new List<Streaming>();

		/// <summary>
		/// EF constructor
		/// </summary>
		/// <param name="id"></param>
		/// <param name="name"></param>
		/// <param name="email"></param>
		/// 

		public Twitcher(string name, string email) : base(name, email)
		{
            subscribers = new List<Subscriber>();
            gifts = new List<Gift>();
        }

        public Twitcher(int id, string name, string email) : this(name, email)
		{
        }

        public Twitcher(int id, string name, string email, IDisplayer displayer) : this(id, name, email)
		{
			this.Displayer = displayer;
        }
		public Streaming createStream(DateTime date, string title)
		{
            Streaming streaming = new Streaming(this, date, title);
            return streaming;
        }
        public void startStream(Streaming stream)
		{
            stream.StartTime = DateTime.Now;
        }

        public void finishStream(Streaming streaming)
        {
            streaming.EndTime = DateTime.Now;
        }


        public string displayGiftAssignements()
		{
			throw new NotImplementedException();
		}

		public void addSubscriber(Subscriber subscriber)
		{
			this.subscribers.Add(subscriber);
		}

		public void WatchGivenGifts()
		{
			foreach (Subscriber subscriber in this.subscribers)
			{
				foreach(Gift gift in subscriber.Gifts)
				{
					this.displayer.Display(gift.ToString());
				}
			}
		}

		public void addGift(Gift gift)
		{
			this.gifts.Add(gift);
		}

        public override string ToString()
        {
            return $"[Twitcher] {base.ToString()}";
        }

        private IDisplayer displayer;
		public List<Subscriber> Subscribers { get => subscribers; set => subscribers = value; }
		public List<Gift> Gifts { get => gifts; set => gifts = value; }

		[NotMapped]
		private IDisplayer Displayer { get => displayer; set => displayer = value; }
        public List<Streaming> Streams { get => streams; set => streams = value; }
    }
}
