﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api
{
	public class Subscriber : Person
	{
		List<Twitcher> twitchers;
		List<Gift> gifts;
		List<Streaming> streams;
		Boolean isConnected;
		string profilePic;

        public Subscriber(string name, string email) : base(name, email)
        {
            this.gifts = new List<Gift>();
            this.twitchers = new List<Twitcher>();
            this.streams = new List<Streaming>();
            this.IsConnected = false;
			this.ProfilePic = "avatar.jpg";
        }

        public Subscriber(int id, string name, string email) : this(name, email)
		{
        }

        public void connectToStream(Streaming streaming)
		{
			streaming.SubscribersConnected.Add(this);
			this.Streams.Add(streaming);
		}

		public override string ToString()
		{
			string s = this.Name + " " + this.Email + "\n";
			this.gifts.ForEach(gift => s += gift.ToString() + "\n");
			return s;
		}

		public List<Gift> Gifts { get => gifts; set => gifts = value; }
		public List<Twitcher> Twitchers { get => twitchers; set => twitchers = value; }
		public bool IsConnected { get => isConnected; set => isConnected = value; }
        public List<Streaming> Streams { get => streams; set => streams = value; }
        public string ProfilePic { get => profilePic; set => profilePic = value; }
    }
}
