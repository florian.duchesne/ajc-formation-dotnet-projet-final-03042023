﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api.Adapters
{
	public interface IReader
	{
		public string Read();
	}
}
