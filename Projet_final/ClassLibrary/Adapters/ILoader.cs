﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api.Adapters
{
    public interface ILoader<T, U>
    {
        public T Get(int id);

        public U GetAll();
    }
}
