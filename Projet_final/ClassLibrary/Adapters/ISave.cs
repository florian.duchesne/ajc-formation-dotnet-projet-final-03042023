﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api.Adapters
{
	public interface ISave<TData>
    {
		public void Add(TData objet);
        public void Remove(int id);
        //void SaveFollower(Subscriber subscriber);

        //void SaveFollowers(List <Subscriber> subscribers);

        //void SaveGift(Gift gift);

        //void SaveGifts(List <Gift> gifts);

    }
}
