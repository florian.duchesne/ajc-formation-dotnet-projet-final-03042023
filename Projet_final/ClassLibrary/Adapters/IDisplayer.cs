﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api.Adapters
{
	public interface IDisplayer
	{
		public void Display(string message);
	}
}
