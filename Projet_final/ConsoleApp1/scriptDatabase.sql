USE [master]
GO
/****** Object:  Database [bdd_projetFinal]    Script Date: 11/04/2023 12:35:53 ******/
CREATE DATABASE [bdd_projetFinal]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'bdd_projetFinal', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.SQLEXPRESS\MSSQL\DATA\bdd_projetFinal.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'bdd_projetFinal_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.SQLEXPRESS\MSSQL\DATA\bdd_projetFinal_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [bdd_projetFinal] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [bdd_projetFinal].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [bdd_projetFinal] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [bdd_projetFinal] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [bdd_projetFinal] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [bdd_projetFinal] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [bdd_projetFinal] SET ARITHABORT OFF 
GO
ALTER DATABASE [bdd_projetFinal] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [bdd_projetFinal] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [bdd_projetFinal] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [bdd_projetFinal] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [bdd_projetFinal] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [bdd_projetFinal] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [bdd_projetFinal] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [bdd_projetFinal] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [bdd_projetFinal] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [bdd_projetFinal] SET  DISABLE_BROKER 
GO
ALTER DATABASE [bdd_projetFinal] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [bdd_projetFinal] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [bdd_projetFinal] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [bdd_projetFinal] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [bdd_projetFinal] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [bdd_projetFinal] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [bdd_projetFinal] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [bdd_projetFinal] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [bdd_projetFinal] SET  MULTI_USER 
GO
ALTER DATABASE [bdd_projetFinal] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [bdd_projetFinal] SET DB_CHAINING OFF 
GO
ALTER DATABASE [bdd_projetFinal] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [bdd_projetFinal] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [bdd_projetFinal] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [bdd_projetFinal] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [bdd_projetFinal] SET QUERY_STORE = ON
GO
ALTER DATABASE [bdd_projetFinal] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [bdd_projetFinal]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 11/04/2023 12:35:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gifts]    Script Date: 11/04/2023 12:35:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gifts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[SubscriberId] [int] NULL,
	[TwitcherId] [int] NOT NULL,
	[Discriminator] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_gifts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Streaming]    Script Date: 11/04/2023 12:35:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Streaming](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StreamerId] [int] NOT NULL,
	[StartTime] [datetime2](7) NOT NULL,
	[EndTime] [datetime2](7) NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_Streaming] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StreamingSubscriber]    Script Date: 11/04/2023 12:35:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StreamingSubscriber](
	[StreamsId] [int] NOT NULL,
	[SubscribersConnectedId] [int] NOT NULL,
 CONSTRAINT [PK_StreamingSubscriber] PRIMARY KEY CLUSTERED 
(
	[StreamsId] ASC,
	[SubscribersConnectedId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[subscribers]    Script Date: 11/04/2023 12:35:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[subscribers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Email] [nvarchar](max) NOT NULL,
	[IsConnected] [bit] NOT NULL,
	[ProfilePic] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_subscribers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubscriberTwitcher]    Script Date: 11/04/2023 12:35:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubscriberTwitcher](
	[SubscribersId] [int] NOT NULL,
	[TwitchersId] [int] NOT NULL,
 CONSTRAINT [PK_SubscriberTwitcher] PRIMARY KEY CLUSTERED 
(
	[SubscribersId] ASC,
	[TwitchersId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[twitchers]    Script Date: 11/04/2023 12:35:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[twitchers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Email] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_twitchers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Index [IX_gifts_SubscriberId]    Script Date: 11/04/2023 12:35:53 ******/
CREATE NONCLUSTERED INDEX [IX_gifts_SubscriberId] ON [dbo].[gifts]
(
	[SubscriberId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_gifts_TwitcherId]    Script Date: 11/04/2023 12:35:53 ******/
CREATE NONCLUSTERED INDEX [IX_gifts_TwitcherId] ON [dbo].[gifts]
(
	[TwitcherId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Streaming_StreamerId]    Script Date: 11/04/2023 12:35:53 ******/
CREATE NONCLUSTERED INDEX [IX_Streaming_StreamerId] ON [dbo].[Streaming]
(
	[StreamerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_StreamingSubscriber_SubscribersConnectedId]    Script Date: 11/04/2023 12:35:53 ******/
CREATE NONCLUSTERED INDEX [IX_StreamingSubscriber_SubscribersConnectedId] ON [dbo].[StreamingSubscriber]
(
	[SubscribersConnectedId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_SubscriberTwitcher_TwitchersId]    Script Date: 11/04/2023 12:35:53 ******/
CREATE NONCLUSTERED INDEX [IX_SubscriberTwitcher_TwitchersId] ON [dbo].[SubscriberTwitcher]
(
	[TwitchersId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Streaming] ADD  DEFAULT (N'') FOR [Title]
GO
ALTER TABLE [dbo].[Streaming] ADD  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[subscribers] ADD  DEFAULT (CONVERT([bit],(0))) FOR [IsConnected]
GO
ALTER TABLE [dbo].[subscribers] ADD  DEFAULT (N'') FOR [ProfilePic]
GO
ALTER TABLE [dbo].[gifts]  WITH CHECK ADD  CONSTRAINT [FK_gifts_subscribers_SubscriberId] FOREIGN KEY([SubscriberId])
REFERENCES [dbo].[subscribers] ([Id])
GO
ALTER TABLE [dbo].[gifts] CHECK CONSTRAINT [FK_gifts_subscribers_SubscriberId]
GO
ALTER TABLE [dbo].[gifts]  WITH CHECK ADD  CONSTRAINT [FK_gifts_twitchers_TwitcherId] FOREIGN KEY([TwitcherId])
REFERENCES [dbo].[twitchers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[gifts] CHECK CONSTRAINT [FK_gifts_twitchers_TwitcherId]
GO
ALTER TABLE [dbo].[Streaming]  WITH CHECK ADD  CONSTRAINT [FK_Streaming_twitchers_StreamerId] FOREIGN KEY([StreamerId])
REFERENCES [dbo].[twitchers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Streaming] CHECK CONSTRAINT [FK_Streaming_twitchers_StreamerId]
GO
ALTER TABLE [dbo].[StreamingSubscriber]  WITH CHECK ADD  CONSTRAINT [FK_StreamingSubscriber_Streaming_StreamsId] FOREIGN KEY([StreamsId])
REFERENCES [dbo].[Streaming] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[StreamingSubscriber] CHECK CONSTRAINT [FK_StreamingSubscriber_Streaming_StreamsId]
GO
ALTER TABLE [dbo].[StreamingSubscriber]  WITH CHECK ADD  CONSTRAINT [FK_StreamingSubscriber_subscribers_SubscribersConnectedId] FOREIGN KEY([SubscribersConnectedId])
REFERENCES [dbo].[subscribers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[StreamingSubscriber] CHECK CONSTRAINT [FK_StreamingSubscriber_subscribers_SubscribersConnectedId]
GO
ALTER TABLE [dbo].[SubscriberTwitcher]  WITH CHECK ADD  CONSTRAINT [FK_SubscriberTwitcher_subscribers_SubscribersId] FOREIGN KEY([SubscribersId])
REFERENCES [dbo].[subscribers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SubscriberTwitcher] CHECK CONSTRAINT [FK_SubscriberTwitcher_subscribers_SubscribersId]
GO
ALTER TABLE [dbo].[SubscriberTwitcher]  WITH CHECK ADD  CONSTRAINT [FK_SubscriberTwitcher_twitchers_TwitchersId] FOREIGN KEY([TwitchersId])
REFERENCES [dbo].[twitchers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SubscriberTwitcher] CHECK CONSTRAINT [FK_SubscriberTwitcher_twitchers_TwitchersId]
GO
USE [master]
GO
ALTER DATABASE [bdd_projetFinal] SET  READ_WRITE 
GO
