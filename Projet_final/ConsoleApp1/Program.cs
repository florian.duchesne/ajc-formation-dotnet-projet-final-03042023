﻿using projet.api;
using System;
using twitch.api;
using projet.infrastructure;
using projet.infrastructure.Console;
using Newtonsoft.Json;
using twitch.api.Adapters;
using menu.api;
using System.Reflection.PortableExecutable;

ConsoleDisplayer displayer = new();
var reader = new ConsoleReader();
Twitcher t = new(1, "Pierre", "pierre@....", displayer);
Twitcher f = new(2, "Florian", "florian@....", displayer);
var twitchers = new TwitcherList();
twitchers.Add(t);
twitchers.Add(f);

t.Gifts = new GiftList()
	{
        new Gift(GiftType.Discount),
        new Gift(GiftType.GameLesson),
        new Gift(GiftType.GameLesson),
        new Gift(GiftType.Offer),
        new Gift(GiftType.Discount),
        new Gift(GiftType.GameLesson),
        new Gift(GiftType.Discount),
        new Gift(GiftType.StreamerMeeting)
    };

var subscribers = new SubscriberList()
{
	new Subscriber(1, "jean", "...@..."),
	new Subscriber(2, "thomas", "...@..."),
	new Subscriber(3, "david", "...@..."),
	new Subscriber(4, "elise", "...@..."),
	new Subscriber(5, "julia", "...@..."),
	new Subscriber(6, "mylene", "...@..."),
};

t.Subscribers = subscribers;

//new RandomSelector(t).AssignGift();

subscribers.ForEach(sub => Console.WriteLine(sub.ToString()));

var gifts = new GiftList(){
        new Gift(GiftType.Discount),
        new Gift(GiftType.GameLesson),
        new Gift(GiftType.GameLesson),
        new Gift(GiftType.Offer),
        new Gift(GiftType.Discount),
        new Gift(GiftType.GameLesson),
        new Gift(GiftType.Discount),
        new Gift(GiftType.StreamerMeeting)
};

ILoader<Gift, GiftList> giftLoader = new ConsoleGiftLoader(gifts);
ILoader<Subscriber, SubscriberList> subscriberLoader = new ConsoleSubscriberLoader(subscribers);
ILoader<Twitcher, TwitcherList> twitcherLoader = new ConsoleTwitcherLoader(twitchers);
ISave<Gift> giftSaver = new ConsoleGiftSaver(gifts);
ISave<Subscriber> subscriberSaver = new ConsoleSubscriberSaver(subscribers);
ISave<Twitcher> twitcherSaver = new ConsoleTwitcherSaver(twitchers);

var menu = new MenuTwitch
(
    displayer,
    reader,
    giftLoader,
    giftSaver,
    subscriberLoader,
    subscriberSaver,
    twitcherLoader,
    twitcherSaver
);

menu.Run();








