﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using twitch.api.Adapters;

namespace projet.api
{
	public class ConsoleReader : IReader
	{

		public string Read()
		{
           return Console.ReadLine();
		}
	}
}