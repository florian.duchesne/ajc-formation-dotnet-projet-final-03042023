﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using twitch.api.Adapters;

namespace projet.api
{
	public class ConsoleDisplayer : IDisplayer
	{
		public void Display(string message)
		{
			Console.WriteLine(message);
		}
	}
}
