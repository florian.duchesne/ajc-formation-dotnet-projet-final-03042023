﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using WebApplication_ProjFinal.Models;

#nullable disable

namespace WebApplication_ProjFinal.Migrations
{
    [DbContext(typeof(DefaultDbContext))]
    [Migration("20230412145933_changestatus")]
    partial class changestatus
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.4")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder);

            modelBuilder.Entity("StreamingSubscriber", b =>
                {
                    b.Property<int>("StreamsId")
                        .HasColumnType("int");

                    b.Property<int>("SubscribersConnectedId")
                        .HasColumnType("int");

                    b.HasKey("StreamsId", "SubscribersConnectedId");

                    b.HasIndex("SubscribersConnectedId");

                    b.ToTable("StreamingSubscriber");
                });

            modelBuilder.Entity("SubscriberTwitcher", b =>
                {
                    b.Property<int>("SubscribersId")
                        .HasColumnType("int");

                    b.Property<int>("TwitchersId")
                        .HasColumnType("int");

                    b.HasKey("SubscribersId", "TwitchersId");

                    b.HasIndex("TwitchersId");

                    b.ToTable("SubscriberTwitcher");
                });

            modelBuilder.Entity("twitch.api.Gift", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<int>("Category")
                        .HasColumnType("int");

                    b.Property<DateTime>("CreationDate")
                        .HasColumnType("datetime2");

                    b.Property<int?>("SubscriberId")
                        .HasColumnType("int");

                    b.Property<int>("TwitcherId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("SubscriberId");

                    b.HasIndex("TwitcherId");

                    b.ToTable("gifts");
                });

            modelBuilder.Entity("twitch.api.Streaming", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<bool?>("ChangeStatus")
                        .HasColumnType("bit");

                    b.Property<DateTime?>("EndTime")
                        .HasColumnType("datetime2");

                    b.Property<DateTime?>("StartTime")
                        .HasColumnType("datetime2");

                    b.Property<int>("Status")
                        .HasColumnType("int");

                    b.Property<int>("StreamerId")
                        .HasColumnType("int");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("StreamerId");

                    b.ToTable("Streaming");
                });

            modelBuilder.Entity("twitch.api.Subscriber", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("IsConnected")
                        .HasColumnType("bit");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ProfilePic")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("subscribers");
                });

            modelBuilder.Entity("twitch.api.Twitcher", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("twitchers");
                });

            modelBuilder.Entity("StreamingSubscriber", b =>
                {
                    b.HasOne("twitch.api.Streaming", null)
                        .WithMany()
                        .HasForeignKey("StreamsId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("twitch.api.Subscriber", null)
                        .WithMany()
                        .HasForeignKey("SubscribersConnectedId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("SubscriberTwitcher", b =>
                {
                    b.HasOne("twitch.api.Subscriber", null)
                        .WithMany()
                        .HasForeignKey("SubscribersId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("twitch.api.Twitcher", null)
                        .WithMany()
                        .HasForeignKey("TwitchersId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("twitch.api.Gift", b =>
                {
                    b.HasOne("twitch.api.Subscriber", "Subscriber")
                        .WithMany("Gifts")
                        .HasForeignKey("SubscriberId");

                    b.HasOne("twitch.api.Twitcher", "Twitcher")
                        .WithMany("Gifts")
                        .HasForeignKey("TwitcherId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Subscriber");

                    b.Navigation("Twitcher");
                });

            modelBuilder.Entity("twitch.api.Streaming", b =>
                {
                    b.HasOne("twitch.api.Twitcher", "Streamer")
                        .WithMany("Streams")
                        .HasForeignKey("StreamerId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Streamer");
                });

            modelBuilder.Entity("twitch.api.Subscriber", b =>
                {
                    b.Navigation("Gifts");
                });

            modelBuilder.Entity("twitch.api.Twitcher", b =>
                {
                    b.Navigation("Gifts");

                    b.Navigation("Streams");
                });
#pragma warning restore 612, 618
        }
    }
}
