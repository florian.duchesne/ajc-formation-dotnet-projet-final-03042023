﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WebApplication_ProjFinal.Migrations
{
    /// <inheritdoc />
    public partial class stream : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Streaming",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StreamerId = table.Column<int>(type: "int", nullable: false),
                    StartTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EndTime = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Streaming", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Streaming_twitchers_StreamerId",
                        column: x => x.StreamerId,
                        principalTable: "twitchers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StreamingSubscriber",
                columns: table => new
                {
                    StreamsId = table.Column<int>(type: "int", nullable: false),
                    SubscribersConnectedId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StreamingSubscriber", x => new { x.StreamsId, x.SubscribersConnectedId });
                    table.ForeignKey(
                        name: "FK_StreamingSubscriber_Streaming_StreamsId",
                        column: x => x.StreamsId,
                        principalTable: "Streaming",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StreamingSubscriber_subscribers_SubscribersConnectedId",
                        column: x => x.SubscribersConnectedId,
                        principalTable: "subscribers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Streaming_StreamerId",
                table: "Streaming",
                column: "StreamerId");

            migrationBuilder.CreateIndex(
                name: "IX_StreamingSubscriber_SubscribersConnectedId",
                table: "StreamingSubscriber",
                column: "SubscribersConnectedId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StreamingSubscriber");

            migrationBuilder.DropTable(
                name: "Streaming");
        }
    }
}
