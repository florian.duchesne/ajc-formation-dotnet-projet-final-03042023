﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WebApplication_ProjFinal.Migrations
{
    /// <inheritdoc />
    public partial class changestatus : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "gifts");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "gifts");

            migrationBuilder.RenameColumn(
                name: "Created",
                table: "gifts",
                newName: "CreationDate");

            migrationBuilder.AlterColumn<DateTime>(
                name: "StartTime",
                table: "Streaming",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<DateTime>(
                name: "EndTime",
                table: "Streaming",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<bool>(
                name: "ChangeStatus",
                table: "Streaming",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Category",
                table: "gifts",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ChangeStatus",
                table: "Streaming");

            migrationBuilder.DropColumn(
                name: "Category",
                table: "gifts");

            migrationBuilder.RenameColumn(
                name: "CreationDate",
                table: "gifts",
                newName: "Created");

            migrationBuilder.AlterColumn<DateTime>(
                name: "StartTime",
                table: "Streaming",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "EndTime",
                table: "Streaming",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "gifts",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "gifts",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }
    }
}
