﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WebApplication_ProjFinal.Migrations
{
    /// <inheritdoc />
    public partial class changestatus2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ChangeStatus",
                table: "Streaming");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "Streaming");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "ChangeStatus",
                table: "Streaming",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "Streaming",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
