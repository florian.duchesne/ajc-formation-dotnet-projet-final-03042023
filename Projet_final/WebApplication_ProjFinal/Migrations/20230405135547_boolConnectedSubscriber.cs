﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WebApplication_ProjFinal.Migrations
{
    /// <inheritdoc />
    public partial class boolConnectedSubscriber : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsConnected",
                table: "subscribers",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsConnected",
                table: "subscribers");
        }
    }
}
