﻿using Microsoft.EntityFrameworkCore;
using twitch.api;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;


namespace WebApplication_ProjFinal.Models
{
	public class DefaultDbContext : DbContext
	{
		public DbSet<Twitcher> twitchers { get; set; }
		public DbSet<Subscriber> subscribers { get; set; }
		public DbSet<Streaming> Streaming { get; set; }
		public DbSet<Gift> gifts { get; set; }

		public DefaultDbContext(DbContextOptions options) : base(options)
		{
		}

		protected DefaultDbContext()
		{
		}

		protected override void OnModelCreating(ModelBuilder builder)
		{
			//builder.Entity<GLearn>();
			//builder.Entity<GOffer>();
			//builder.Entity<GShare>();
			//builder.Entity<GDiscount>();

			base.OnModelCreating(builder);
		}
		//protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		//{
		//	optionsBuilder.Configuration.GetConnectionString("Default");
		//	//optionsBuilder.UseSqlServer("connection_string_here");
		//}

	}
}
