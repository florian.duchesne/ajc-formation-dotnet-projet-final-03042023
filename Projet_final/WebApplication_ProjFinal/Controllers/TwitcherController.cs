﻿using Azure.Core;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using twitch.api;
using WebApplication_ProjFinal.Models;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace WebApplication_ProjFinal.Controllers
{
    public class TwitcherController : Controller
    {
        private readonly DefaultDbContext context;

        public TwitcherController(DefaultDbContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult createStream()
        {
            return View("streamForm");
        }

        [HttpPost]
        public IActionResult createStream(Streaming streaming)
        {
            var twitcher = context.twitchers.Find(1);
            if (streaming.Id == 0)
            {
                streaming.Streamer = twitcher;
                this.context.Streaming.Add(streaming);
            }
            else
            {
                Streaming streamToUpdate = this.context.Streaming.Find(streaming.Id);
                streamToUpdate.Title = streaming.Title;
                streamToUpdate.StartTime = streaming.StartTime;

                if (streaming.ChangeStatus == true)
                {
                    var actualStatus = streamToUpdate.getStatus();

                    if (actualStatus == StatutStream.AVenir)
                    {
                        streamToUpdate.StartTime = DateTime.Now;
                    }
                    if (actualStatus == StatutStream.EnCours)
                    {
                        streamToUpdate.EndTime = DateTime.Now;
                    }
                }

            }
            this.context.SaveChanges();

            return RedirectToAction("Index", "Home");
        }

        public IActionResult editStream(int  id)
        {
            Streaming stream = context.Streaming.Find(id);
            stream.Status = stream.getStatus();


            return View("streamForm", stream);
        }
    }
}
