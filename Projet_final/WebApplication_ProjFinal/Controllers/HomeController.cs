﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using twitch.api.Adapters;
using twitch.api;
using WebApplication_ProjFinal.Models;

namespace WebApplication_ProjFinal.Controllers
{
	public class HomeController : Controller
	{
		private readonly ILogger<HomeController> _logger;
		private readonly DefaultDbContext context;

		public HomeController(ILogger<HomeController> logger, DefaultDbContext context)
		{
			_logger = logger;
			this.context = context;
		}

		public IActionResult Index()
		{
            var query = from item in this.context.Streaming
                        select item;

            var streamings = query.ToList(); // Traduction, requete linq en requete sql
            foreach (var item in streamings)
            {
				item.Status = item.getStatus();
            }
            return View(streamings);
		}

		public IActionResult Privacy()
		{
			return View();
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
}