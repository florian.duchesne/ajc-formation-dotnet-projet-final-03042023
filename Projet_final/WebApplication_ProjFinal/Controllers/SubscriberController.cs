﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using twitch.api;
using WebApplication_ProjFinal.Models;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace WebApplication_ProjFinal.Controllers
{
	public class SubscriberController : Controller
	{
		private readonly DefaultDbContext context;

		public SubscriberController(DefaultDbContext context)
		{
			this.context = context;
		}

		public IActionResult Index()
		{
			var query = from item in this.context.subscribers
						where item.IsConnected == true
						select item;

            var subscribers = query.ToList(); // Traduction, requete linq en requete sql

            ViewData["info"] = "connectés";
            ViewData["subscribers"] = subscribers;
			return View();
		}

        public IActionResult filterFollowers()
        {
            // J'ai mis en place la bdd pour pouvoir avoir plusieurs twitchers,
            // mais pour l'instant on est dans un système où il n'y en a qu'un

            List<Subscriber> subscribers = new List<Subscriber>();

            Twitcher twitcher = context.twitchers.Find(1);
            int idStreamer = 1;

            var selectValue = this.Request.Form["followers"];

            if (selectValue == "all")
            {
                var query = from item in this.context.subscribers
                        where item.Twitchers.Contains(twitcher)
                        select item;
                subscribers = query.ToList();
                ViewData["info"] = "(tous)";
            }
            if (selectValue == "connected")
            {
                var query = from item in this.context.subscribers
                        where item.IsConnected == true
                        where item.Twitchers.Contains(twitcher)
                        select item;
                subscribers = query.ToList();
                ViewData["info"] = "connectés";
            }
            if (selectValue == "favfollowers")
            {
                int nbStreams = this.context.Streaming.Count(s => s.Streamer.Id == idStreamer);
                double seuilFidelite = Math.Ceiling(nbStreams * 0.7);

                // requete linq à établir :
                // parmi les streams et le follower
                // join
                // where stream.streamer = stream
                // select stream.followers
                // où follower.streams.count = seuil 

                var query = from stream in this.context.Streaming
                            where stream.Streamer == twitcher
                            from follower in stream.SubscribersConnected
                            group follower by follower into followerGroup
                            where followerGroup.Count() >= seuilFidelite
                            select followerGroup.Key;

                subscribers = query.ToList();
                ViewData["info"] = "favoris";
            }
            ViewData["subscribers"] = subscribers;
            return View("Index");
        }
    }
}
